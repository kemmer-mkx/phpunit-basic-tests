<?php

class AlgumasFunc
{
    public static function geraUm() {
        return 1;
    }

    public static function geraArray_Letras($letras) {
        $arrayLetras = [];

        for ($i=0; $i < strlen($letras); $i++) { 
            $arrayLetras[] = substr($letras, $i, 1);
        }

        return $arrayLetras;
    }

    public static function geraArray_Ordena($dadosArray, $tipo) {

        if($tipo == 'numerico')
            sort($dadosArray, SORT_NUMERIC);

        elseif($tipo == 'string')
            sort($dadosArray, SORT_NATURAL);
        else
            sort($dadosArray);

        return $dadosArray;
    }

    public static function geraArray_Ordena2($dadosArray) {

        for ($i=0; $i < count($dadosArray); $i++) { 
            
            $valor = $dadosArray[$i];
            for ($j = $i; $j < count($dadosArray); $j++) { 
                if($dadosArray[$i] > $dadosArray[$j]) {
                    $temp = $dadosArray[$i];
                    $dadosArray[$i] = $dadosArray[$j];
                    $dadosArray[$j] = $temp;
                }
            }
        }

        return $dadosArray;
    }

    public static function duplicaNumero($numero) {
        if(!is_numeric($numero))
            throw new InvalidArgumentException('Erro', 10);
    }
}