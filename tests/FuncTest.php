<?php
declare(strict_types=1);

use PHPUnit\Framework\TestCase;

/**
 * - Testes normais
 * - Testes de exceções
 * - Testes erros do PHP
 */


final class FuncTest extends TestCase
{
    public function testTestaFalso()
    {
        $this->assertEquals(9, 9);
    }

    public function testGeraUm() {
        $resultado = AlgumasFunc::geraUm();
        $this->assertEquals(1, $resultado);
    }

    public function testGeraArrayLetras() {
        $resultado = AlgumasFunc::geraArray_Letras('litera');

        $this->assertEquals([
            'l', 'i', 't', 'e', 'r', 'a',
        ], $resultado);

        return $resultado;
    }

    /**
     * @depends testGeraArrayLetras 
     */
    public function testGeraArrayOrdena(array $dados) {
        $resultado = AlgumasFunc::geraArray_Ordena($dados, 'string');
        $this->assertEquals([
            'a', 'e', 'i', 'l', 'r', 't'
        ], $resultado, 'Não ordenou corretamente');
    }

    public function testGeraArrayOrdena2() {
        $resultado = AlgumasFunc::geraArray_Ordena2([
            92,3,9,3,4,8,2,7
        ]);
        $this->assertEquals([2,3,3,4,7,8,9,92], $resultado);
    }

    /**
     * @expectedException InvalidArgumentException
     */
    public function testException() {
        AlgumasFunc::duplicaNumero('a');
    }

    /**
     * @expectedException InvalidArgumentException
     * @expectedExceptionMessage Erro
     * @expectedExceptionCode 10
     */
    public function testException2() {
        AlgumasFunc::duplicaNumero('a');
    }
}
